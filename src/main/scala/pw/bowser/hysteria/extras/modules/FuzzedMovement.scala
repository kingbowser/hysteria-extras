package pw.bowser.hysteria.extras.modules

import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.network.PacketSendEvent
import net.minecraft.network.play.client.C03PacketPlayer

import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.commands.HysteriaCommand
import scala.util.Random
import pw.bowser.hysteria.module.internal.HysteriaModule

/**
 * Date: 5/16/14
 * Time: 10:52 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "FuzzedMovement",
  version     = "0.1",
  description = "Varies X-Z movement values by a set range randomly.",
  commands    = Array("fzm")
)
class FuzzedMovement extends HysteriaModule with TogglesMixin {

  private val packetSendListener  = EventClient[PacketSendEvent] {event =>
    event.packet match {
      case p: C03PacketPlayer.C04PacketPlayerPosition =>
        event.setPacket(
          new C03PacketPlayer.C04PacketPlayerPosition(p.func_149464_c() + randomVariedDouble, // X
                                                      p.func_149467_d(),                      // Min. Y
                                                      p.func_149471_f(),                      // Y
                                                      p.func_149472_e() + randomVariedDouble, // Z
                                                      p.func_149465_i())                      // Grounded
        )
      case p: C03PacketPlayer.C06PacketPlayerPosLook =>
        event.setPacket(
          new C03PacketPlayer.C06PacketPlayerPosLook(p.func_149464_c() + randomVariedDouble,  // X
                                                     p.func_149467_d(),                       // Min. Y
                                                     p.func_149471_f(),                       // Y
                                                     p.func_149472_e() + randomVariedDouble,  // Z
                                                     p.func_149462_g(),                       // Yaw
                                                     p.func_149470_h(),                       // Pitch
                                                     p.func_149465_i())                       // Grounded
        )
      case _ => // Ignore
    }
  }

  private var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()

    command = Command(Array("fzm"),
                      ToggleFlag(Array("@if_none"), this),
                      ConfigurationFlag(Array("v", "variance"), configuration, "variance", ConfigurationFlag.Transforms.DOUBLE))

    configuration.setPropertyIfNew("variance", 1.2D)
  }

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
  }

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return dn
   */
  override def distinguishedName: String = "pw.hysteria.fuzzed_movement"

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "FuzzedMovement"

  override def conflictingToggles: List[String] = List("pw.hysteria.paralyze")

  /**
   * Called when the toggles is enabled or required
   * The implementation should store logic to start and enable functionality here.
   */
  override def enable(): Unit = {
    eventSystem.subscribe(classOf[PacketSendEvent], packetSendListener)
    super.enable()
  }

  /**
   * Called when the toggles is disabled or suppressed.
   * The implementation should store logic to halt and disable functionality here.
   */
  override def disable(): Unit = {
    eventSystem.unSubscribe(classOf[PacketSendEvent], packetSendListener)
    super.disable()
  }

  private def randomVariedDouble: Double = Random.nextDouble() * configuration.getDouble("variance")

}
