package pw.bowser.hysteria.extras.modules

import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.minecraft.TickReceiver
import net.minecraft.network.play.client.C03PacketPlayer
import scala.collection.JavaConversions._
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 3/21/14
 * Time: 9:32 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Paralyze",
  version     = "0.1",
  commands    = Array("par"),
  description = "Prevent players you are standing in from moving"
)
class Paralyze extends HysteriaModule with TogglesMixin with TickReceiver {


  var command: HysteriaCommand = Command(Array("par"), ToggleFlag(Array("@if_none", "t"), this))

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // TickReceiver Implementation
  //--------------------------------------------------------------------------------------------------------------------

  def onTick(): Unit = if(isEnabled && minecraft.theWorld != null) {
    for(i <- 1 to 400) minecraftToolkit.sendPacket(new C03PacketPlayer(false))
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Paralyze"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.paralyze"

  override def canPersist: Boolean = false

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
    minecraft.getNetHandler.getNetworkManager.getOutboundPacketsQueue.removeAll(minecraft.getNetHandler.getNetworkManager.getOutboundPacketsQueue.filter(_.isInstanceOf[C03PacketPlayer]))
  }
}
