package pw.bowser.hysteria.extras.modules

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.minecraft.player.PreMotionUpdate
import pw.bowser.hysteria.eventsimpl.network.PacketSendEvent
import net.minecraft.network.play.client.{C07PacketPlayerDigging, C08PacketPlayerBlockPlacement, C03PacketPlayer}

import scala.collection.mutable
import net.minecraft.network.Packet
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.minecraft.RenderJob
import org.lwjgl.opengl.GL11
import net.minecraft.client.renderer.entity.RenderManager

/**
 * Date: 5/16/14
 * Time: 10:52 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria.contrib",
  name        = "MovementBuffer",
  version     = "0.1",
  description = "Buffers all movement while on and then flushes when disabled",
  commands    = Array("mbf")
)
class MovementBuffer extends HysteriaModule with TogglesMixin with RenderJob {

  private var startingPos         = (0D, 0D, 0D)

  private val packetBuffer        = new mutable.Queue[Packet]()

  private val packetSendListener  = EventClient[PacketSendEvent] {event =>
    event.packet match {
      case p: C03PacketPlayer =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case p: C08PacketPlayerBlockPlacement =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case p: C07PacketPlayerDigging =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case _ => // Ignored
    }
  }

  private val preMotionListener   = EventClient[PreMotionUpdate] {event =>
    if(minecraftToolkit.isInWorld
        && minecraftToolkit.thePlayer.motionX == 0
        && minecraftToolkit.thePlayer.motionY >= -0.2
        && minecraftToolkit.thePlayer.motionZ == 0)
      event.cancel()
  }

  private val command = Command(Array("mbf"), ToggleFlag(Array("@if_none"), this))



  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
  }

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return dn
   */
  override def distinguishedName: String = "pw.hysteria.deferred_movement"

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "MovementBuffer"


  override def statusText: Option[String] = Some(s"${packetBuffer.size}")


  override def conflictingToggles: List[String] = List("pw.hysteria.paralyze", "pw.hysteria.strided_movement")

  /**
   * Called when the toggles is enabled or required
   * The implementation should store logic to start and enable functionality here.
   */
  override def enable(): Unit = {
    packetBuffer.clear()
    startingPos = (minecraftToolkit.thePlayer.posX, minecraftToolkit.thePlayer.boundingBox.minY, minecraftToolkit.thePlayer.posZ)

    eventSystem.subscribe(classOf[PacketSendEvent], packetSendListener)
    eventSystem.subscribe(classOf[PreMotionUpdate], preMotionListener)
//    worldRenderQueue.register(this)
    super.enable()
  }

  /**
   * Called when the toggles is disabled or suppressed.
   * The implementation should store logic to halt and disable functionality here.
   */
  override def disable(): Unit = {
    eventSystem.unSubscribe(classOf[PacketSendEvent], packetSendListener)
    eventSystem.unSubscribe(classOf[PreMotionUpdate], preMotionListener)

    while(!packetBuffer.isEmpty){
      val packet = packetBuffer.dequeue()

      minecraftToolkit.sendPacket(packet)
    }

//    worldRenderQueue.remove(this)
    super.disable()
  }

  override def render(): Unit = {
    GL11.glPushMatrix()
    GL11.glDisable(GL11.GL_DEPTH_TEST)
    GL11.glLineWidth(2)
    GL11.glColor4f(1, 1, 1, 1)
    GL11.glBegin(GL11.GL_LINES)
    GL11.glVertex3d(startingPos._1 - .5 - RenderManager.renderPosX, startingPos._2 - RenderManager.renderPosY, startingPos._3 - .5 - RenderManager.renderPosZ)
    GL11.glVertex3d(startingPos._1 + .5 - RenderManager.renderPosX, startingPos._2 - RenderManager.renderPosY, startingPos._3 + .5 - RenderManager.renderPosZ)
    GL11.glVertex3d(startingPos._1 - .5 - RenderManager.renderPosX, startingPos._2 - RenderManager.renderPosY, startingPos._3 + .5 - RenderManager.renderPosZ)
    GL11.glVertex3d(startingPos._1 + .5 - RenderManager.renderPosX, startingPos._2 - RenderManager.renderPosY, startingPos._3 - .5 - RenderManager.renderPosZ)
    GL11.glEnd()
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    GL11.glPopMatrix()
  }
}
