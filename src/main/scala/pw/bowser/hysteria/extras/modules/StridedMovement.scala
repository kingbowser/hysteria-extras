package pw.bowser.hysteria.extras.modules

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.minecraft.player.PreMotionUpdate
import pw.bowser.hysteria.eventsimpl.network.PacketSendEvent
import net.minecraft.network.play.client.{C07PacketPlayerDigging, C08PacketPlayerBlockPlacement, C03PacketPlayer}

import net.minecraft.network.Packet
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.minecraft.abstractionlayer.TimeUtil
import pw.bowser.hysteria.commands.HysteriaCommand
import scala.collection.mutable

/**
 * Date: 5/16/14
 * Time: 10:52 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "StridedMovement",
  version     = "0.1",
  description = "Buffers all movement while on and then flushes every few ticks",
  commands    = Array("stm")
)
class StridedMovement extends HysteriaModule with TogglesMixin {

  private val flushTimer          = new TimeUtil

  private val packetBuffer        = new mutable.Queue[Packet]
  private val packetsAllow        = new mutable.HashSet[Packet]

  private val packetSendListener  = EventClient[PacketSendEvent] {event =>
    event.packet match {
      case p: C03PacketPlayer if !packetsAllow.contains(p) =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case p: C08PacketPlayerBlockPlacement if !packetsAllow.contains(p) =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case p: C07PacketPlayerDigging if !packetsAllow.contains(p) =>
        packetBuffer.enqueue(event.packet)
        event.cancel()
      case p: Packet if packetsAllow.contains(p)=>
        packetsAllow -= p
      case _ => // Ignore
    }

    if(flushTimer.hasPassed(configuration.getLong("stride_interval"))){
      flushPacketBuffer()
    }
  }

  private val preMotionListener   = EventClient[PreMotionUpdate] {event =>
    if(minecraftToolkit.isInWorld
        && minecraftToolkit.thePlayer.motionX == 0
        && minecraftToolkit.thePlayer.motionY >= -0.2
        && minecraftToolkit.thePlayer.motionZ == 0)
      event.cancel()
  }

  private var command: HysteriaCommand = null


  override def initModule(): Unit = {
    super.initModule()

    command = Command(Array("stm"),
                      ToggleFlag(Array("@if_none"), this),
                      ConfigurationFlag(Array("i", "interval"), configuration, "stride_interval", ConfigurationFlag.Transforms.LONG))

    configuration.setPropertyIfNew("stride_interval", 2300L)
  }

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
  }

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return dn
   */
  override def distinguishedName: String = "pw.hysteria.strided_movement"

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "StridedMovement"


  override def statusText: Option[String] = Some(s"${packetBuffer.size}")


  override def conflictingToggles: List[String] = List("pw.hysteria.paralyze", "pw.hysteria.deferred_movement")


  override def canPersist: Boolean = false

  /**
   * Called when the toggles is enabled or required
   * The implementation should store logic to start and enable functionality here.
   */
  override def enable(): Unit = {
    packetBuffer.clear()
    packetsAllow.clear()

    eventSystem.subscribe(classOf[PacketSendEvent], packetSendListener)
    eventSystem.subscribe(classOf[PreMotionUpdate], preMotionListener)
    super.enable()
  }

  /**
   * Called when the toggles is disabled or suppressed.
   * The implementation should store logic to halt and disable functionality here.
   */
  override def disable(): Unit = {
    eventSystem.unSubscribe(classOf[PacketSendEvent], packetSendListener)
    eventSystem.unSubscribe(classOf[PreMotionUpdate], preMotionListener)

    flushPacketBuffer()
    super.disable()
  }

  private def flushPacketBuffer(): Unit = {
    while(!packetBuffer.isEmpty){
      val packet = packetBuffer.dequeue()
      packetsAllow += packet
      minecraftToolkit.sendPacket(packet)
    }
  }
}
