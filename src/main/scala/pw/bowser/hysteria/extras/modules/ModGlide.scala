package pw.bowser.hysteria.extras.modules

import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.world.WorldEnterEvent
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.module.ModuleWithServices

/**
 * Date: 3/5/14
 * Time: 5:57 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria.shusako",
  name        = "glide",
  version     = "0.1",
  commands    = Array("glide", "gl"),
  description = "Allows the player to glide as they fall"
)
class ModGlide extends ModuleWithServices with HysteriaServices with TogglesMixin with TickReceiver {

  // Fields
  //--------------------------------------------------------------------------------------------------------------------

  private var ticksGliding = 0

  // Misc
  //--------------------------------------------------------------------------------------------------------------------

  /*
   * Listen for world changes to reset glide counter
   */
  private val eventWorldEnter = EventClient[WorldEnterEvent]({event => ticksGliding = 0}, 1)

  private val commandGlide =  Command(Array("gl", "glide"), ToggleFlag(Array("@if_none", "@default"), this) )

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def enableModule(): Unit = {
    eventSystem.subscribe(classOf[WorldEnterEvent], eventWorldEnter)
    commandDispatcher.registerCommand(commandGlide)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    eventSystem.unSubscribe(classOf[WorldEnterEvent], eventWorldEnter)
    commandDispatcher.unRegisterCommand(commandGlide)
    toggleRegistry.disenrollToggleable(this)
  }

  // TickReceiver implementation
  //--------------------------------------------------------------------------------------------------------------------

  def onTick(): Unit = {
    if(minecraft.theWorld != null && !minecraft.thePlayer.isOnLadder){
      ticksGliding = if(minecraft.thePlayer.onGround || minecraft.thePlayer.motionY >= 0 || minecraft.thePlayer.isInWater)
        0 else ticksGliding + 1

      if(ticksGliding > 3)
        minecraft.thePlayer.motionY += (if(ticksGliding >= 202) 0 else 1) * (0.05F + (0.001F * (ticksGliding / 100)))
    }
  }

  // Toggles implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */

  def displayName: String = "Glide"

  def distinguishedName: String = "pw.hysteria.shusako.glide"

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
  }

}
