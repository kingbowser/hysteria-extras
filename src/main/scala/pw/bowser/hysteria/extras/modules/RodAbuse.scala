package pw.bowser.hysteria.extras.modules

import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.minecraft.TickReceiver
import net.minecraft.init.Items
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 4/16/14
 * Time: 11:44 PM
 */
@ModuleManifest(
  groupId = "pw.bowser",
  name = "Rod Abuse",
  version = "0.1",
  commands = Array("rabuse"),
  description = "No."
)
class RodAbuse extends HysteriaModule with TogglesMixin with TickReceiver {

  var command: HysteriaCommand = Command(Array("rabuse"), ToggleFlag(Array("@if_none"), this))

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "RodAbuse"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.rod_abuse"

  override def canPersist: Boolean = false

  override def disable(): Unit = {
    tickService.remove(this)
  }

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def onTick(): Unit = if(minecraftToolkit.thePlayer != null && minecraftToolkit.inventoryToolkit.currentItemStack != null && minecraftToolkit.inventoryToolkit.currentItemStack.getItem == Items.fishing_rod) {
//    for(x <- 0 to 900) minecraft.playerController.sendUseItem(minecraftToolkit.thePlayer, minecraft.theWorld, minecraftToolkit.inventoryToolkit.currentItemStack)
    for(x <- 0 to 1000) minecraftToolkit.sendPacket(new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, minecraftToolkit.inventoryToolkit.currentItemStack, 0F, 0F, 0F))
  }

}
