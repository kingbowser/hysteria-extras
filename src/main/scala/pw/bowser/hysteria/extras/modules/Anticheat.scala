package pw.bowser.hysteria.extras.modules

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import net.minecraft.network.play.client.{C03PacketPlayer, C0BPacketEntityAction}
import pw.bowser.hysteria.minecraft.abstractionlayer.TimeUtil
import pw.bowser.hysteria.eventsimpl.network.PacketSendEvent
import pw.bowser.hysteria.events.EventClient
import net.minecraft.network.Packet
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.hysteria.module.internal.HysteriaModule

/**
 * Date: 4/16/14
 * Time: 11:44 PM
 */
@ModuleManifest(
  groupId = "pw.bowser",
  name = "noac",
  version = "0.1",
  commands = Array("noac"),
  description = "No."
)
class Anticheat extends HysteriaModule with TogglesMixin with TickReceiver {

  var command: HysteriaCommand = Command(Array("noac"), ToggleFlag(Array("@if_none"), this))

  private val timer = new TimeUtil

  // Event Client
  //--------------------------------------------------------------------------------------------------------------------

  private var sentPacket: Packet = null

  private val onPacketSend  = EventClient[PacketSendEvent]({event =>
    event.packet match {
      case mu @ (_:C03PacketPlayer.C04PacketPlayerPosition | _:C03PacketPlayer.C06PacketPlayerPosLook) if !mu.eq(sentPacket) && isEnabled =>
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 1))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 2))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 1))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 2))
        sentPacket = mu
        minecraftToolkit.sendPacket(mu)
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 1))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 2))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 1))
        minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 2))

        event.cancel()
      case _ =>
    }
  })

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
    eventSystem.unSubscribe(classOf[PacketSendEvent], onPacketSend)
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
    eventSystem.subscribe(classOf[PacketSendEvent], onPacketSend)
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Anticheat"

  def distinguishedName: String = "pw.hysteria.anticheat_bypass"

  // Tick Receiver
  //--------------------------------------------------------------------------------------------------------------------

  private var lastWasSneak = false

  override def onTick(): Unit = if(minecraft.theWorld != null && timer.hasPassed(50)) {
    if(lastWasSneak)
      minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 2))
    else
      minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, 1))

    lastWasSneak = !lastWasSneak
  }
}
