package pw.bowser.hysteria.extras

import pw.bowser.spm.SPMPlugin
import pw.bowser.spm.manifest.YAMLManifest
import pw.bowser.libhysteria.modules.ModuleLoader
import com.google.inject.Inject
import pw.bowser.hysteria.extras.modules._

/**
 * Date: 6/23/14
 * Time: 5:17 PM
 */
class HysteriaExtras @Inject() (modLoader: ModuleLoader) extends SPMPlugin[YAMLManifest] {
  override def pluginLoaded(manifest: YAMLManifest): Unit = {
    modLoader.loadModule(classOf[Anticheat])
    modLoader.loadModule(classOf[FuzzedMovement])
    modLoader.loadModule(classOf[ModGlide])
    modLoader.loadModule(classOf[MovementBuffer])
    modLoader.loadModule(classOf[Paralyze])
    modLoader.loadModule(classOf[RodAbuse])
    modLoader.loadModule(classOf[StridedMovement])
  }
}
